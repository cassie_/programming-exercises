# frozen_string_literal: true

require 'rspec/autorun'
require 'open3'

describe 'calculate' do
  let(:path) { ".:#{ENV['PATH']}" }
  let!(:env) { { 'PATH' => path } }
  it 'can calculate a tip' do
    output, _status = Open3.capture2e(env, 'tip_calculator',
                                      stdin_data: "200\n15")

    expected_output = <<~OUTPUT
      What is the bill?
      What is the tip percentage?
      What is the bill? $200
      What is the tip percentage? 15%
      The tip is $30.00
      The total is $230.00
    OUTPUT

    expect(output).to eq(expected_output)
  end

  it 'can handle % input' do
    output, _status = Open3.capture2e(env, 'tip_calculator',
                                      stdin_data: "200\n15%")

    expected_output = <<~OUTPUT
      What is the bill?
      What is the tip percentage?
      What is the bill? $200
      What is the tip percentage? 15%
      The tip is $30.00
      The total is $230.00
    OUTPUT

    expect(output).to eq(expected_output)
  end
end
